/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Employee.Employee;
import Business.Organization.AdminOrganization;
import Business.Organization.DoctorOrganization;
import Business.Organization.LabAssistantOrganization;
import Business.UserAccount.UserAccount;

/**
 *
 * @author ran
 */
public class ConfigureABusiness {
    
    public static Business configure(){
        // Three roles: LabAssistant, Doctor, Admin
        
        Business business = Business.getInstance();

        AdminOrganization adminOrganization = new AdminOrganization();
        business.getOrganizationDirectory().getOrganizationList().add(adminOrganization);
        Employee employee = new Employee();
        employee.setName("Jack");
        UserAccount userAccount = new UserAccount();
        userAccount.setUsername("admin");
        userAccount.setPassword("admin");
        userAccount.setRole("admin");
        userAccount.setEmployee(employee);
        adminOrganization.getEmployeeDirectory().getEmployeeList().add(employee);
        adminOrganization.getUserAccountDirectory().getUserAccountList().add(userAccount);
        
        DoctorOrganization docOrg = new DoctorOrganization();
        business.getOrganizationDirectory().getOrganizationList().add(docOrg);
        Employee emp = new Employee();
        emp.setName("jackie");
        UserAccount userAccount1 = new UserAccount();
        userAccount1.setUsername("doctor");
        userAccount1.setPassword("doctor");
        userAccount1.setRole("doctor");
        userAccount1.setEmployee(emp);
        docOrg.getEmployeeDirectory().getEmployeeList().add(emp);
        docOrg.getUserAccountDirectory().getUserAccountList().add(userAccount1);
        
        LabAssistantOrganization labOrg = new LabAssistantOrganization();
        business.getOrganizationDirectory().getOrganizationList().add(labOrg);
        Employee emp1 = new Employee();
        emp1.setName("Ben");
        UserAccount user1 = new UserAccount();
        user1.setUsername("lab");
        user1.setPassword("lab");
        user1.setRole("lab");
        user1.setEmployee(emp1);
        labOrg.getEmployeeDirectory().getEmployeeList().add(emp1);
        labOrg.getUserAccountDirectory().getUserAccountList().add(user1);
        
        return business;
    }

}